#!/usr/bin/env python
from distutils.core import setup

setup(name='Mol2',
      version='1.0',
      description='Mol2 tools',
      long_description="""\
Mol2: a class to manage mol2 files

Written (2008) by P. Tuffery, INSERM, France

This program is used in production since 2011 at the RPBS structural
bioinformatics platform. 

""",
      author='P. Tuffery',
      author_email=['pierre.tuffery@univ-paris-diderot.fr'],
      url='http://bioserv.rpbs.univ-paris-diderot.fr',
      scripts = [],
      packages = ['Mol2'],
      classifiers=['License :: OSI Approved :: GNU General Public License (GPL)',
                   'Operating System :: Unix',
                   'Programming Language :: Python',
                   'Topic :: Scientific/Engineering :: Bio-Informatics',
                   'Topic :: Software Development :: Libraries :: Python Modules'],
      license='GNU General Public License (GPL)'
     )

